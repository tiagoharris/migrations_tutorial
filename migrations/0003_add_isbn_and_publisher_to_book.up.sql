BEGIN;

ALTER TABLE `book`

ADD COLUMN `isbn` varchar(13) NOT NULL,
ADD COLUMN `publisher` varchar(20) NOT NULL;

COMMIT;